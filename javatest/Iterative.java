package javatest;

import java.util.Scanner;

public class Iterative {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the integer");
        int n = scan.nextInt();
        if (n < 10) {
            System.out.println("This number is too small");
        } else {
            System.out.println("This number is big enough");
        }
    }
    }

