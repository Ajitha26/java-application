package javatest;

import java.util.Scanner;

public class Switch {
    public static void main(String[] args) {
        int number;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number");
        number = scan.nextInt();
        switch (number) {
            case 1:
                System.out.println("Got 1");
                break;
            case 2:
                System.out.println("got 2");
                break;
            case 3:
                System.out.println("got 3");
                break;
            case 66:
                System.out.println("got 66");
                break;
            default:
                System.out.println("wrong number");

        }

    }
}