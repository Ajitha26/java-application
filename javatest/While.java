package javatest;

import java.util.Scanner;

public class While {
    public static void main(String[] args) {
        int i = 0;
        Scanner scan = new Scanner(System.in);

        while (i != 5) {
            System.out.println("Enter the value");
            int s = scan.nextInt();
            if (s == 5) {
                System.out.println("got it");
            } else {
                System.out.println("not got it");

            }
        }
    }
}